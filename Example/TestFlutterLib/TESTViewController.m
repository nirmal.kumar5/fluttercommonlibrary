//
//  TESTViewController.m
//  TestFlutterLib
//
//  Created by Nirmalkumar Selvaraj on 05/19/2022.
//  Copyright (c) 2022 Nirmalkumar Selvaraj. All rights reserved.
//

#import "TESTViewController.h"
#import <TestFlutterLib/TestLog.h>

@interface TESTViewController ()

@end

@implementation TESTViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    TestLog *log = [[TestLog alloc] init];
    [log testLog];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
