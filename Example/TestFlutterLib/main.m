//
//  main.m
//  TestFlutterLib
//
//  Created by Nirmalkumar Selvaraj on 05/19/2022.
//  Copyright (c) 2022 Nirmalkumar Selvaraj. All rights reserved.
//

@import UIKit;
#import "TESTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TESTAppDelegate class]));
    }
}
