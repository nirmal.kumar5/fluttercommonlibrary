//
//  TestLog.h
//  TestFlutterLib
//
//  Created by Nirmal Kumar Selvaraj on 19/05/22.
//

#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestLog : NSObject

- (void)testLog;

@end

NS_ASSUME_NONNULL_END
