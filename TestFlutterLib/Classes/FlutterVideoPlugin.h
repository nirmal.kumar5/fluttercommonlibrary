//
//  FlutterVideoPlugin.h
//  FlutterVideoPlugin
//
//  Created by Rumman Mahmud on 21/6/21.
//  Copyright © 2021 Grab. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for FlutterVideoPlugin.
FOUNDATION_EXPORT double FlutterVideoPluginVersionNumber;

//! Project version string for FlutterVideoPlugin.
FOUNDATION_EXPORT const unsigned char FlutterVideoPluginVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FlutterVideoPlugin/PublicHeader.h>


#import <FLTVideoPlayerPlugin.h>
#import <messages.h>
