# TestFlutterLib

[![CI Status](https://img.shields.io/travis/Nirmalkumar Selvaraj/TestFlutterLib.svg?style=flat)](https://travis-ci.org/Nirmalkumar Selvaraj/TestFlutterLib)
[![Version](https://img.shields.io/cocoapods/v/TestFlutterLib.svg?style=flat)](https://cocoapods.org/pods/TestFlutterLib)
[![License](https://img.shields.io/cocoapods/l/TestFlutterLib.svg?style=flat)](https://cocoapods.org/pods/TestFlutterLib)
[![Platform](https://img.shields.io/cocoapods/p/TestFlutterLib.svg?style=flat)](https://cocoapods.org/pods/TestFlutterLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TestFlutterLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TestFlutterLib'
```

## Author

Nirmalkumar Selvaraj, nirmal.kumar@grabtaxi.com

## License

TestFlutterLib is available under the MIT license. See the LICENSE file for more info.
