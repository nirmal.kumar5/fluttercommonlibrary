#
# Be sure to run `pod lib lint TestFlutterLib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TestFlutterLib'
  s.version          = '0.1.3'
  s.summary          = 'A Test pod file to check flutter dependecies.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = "Test pod to test flutter libraries to integrated with pax"

  s.homepage         = 'https://gitlab.com/nirmal.kumar5/fluttercommonlibrary'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Nirmalkumar Selvaraj' => 'nirmal.kumar@grabtaxi.com' }
  s.source           = { :git =>'https://gitlab.com/nirmal.kumar5/fluttercommonlibrary.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  #s.source_files = 'TestFlutterLib/Classes/**/*'
  
  # s.resource_bundles = {
  #   'TestFlutterLib' => ['TestFlutterLib/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'Flutter'
  # s.dependency 'AFNetworking', '~> 2.3'
     # NOTE: To be used only for PAX app integration
  #s.subspec 'PAX' do |ss|
    s.source_files = 'TestFlutterLib/Classes/**/*'


    # SnapKit is already linked to ProxyLibrary inside PAX so it is not allowed to add it as dependency.
    # As a workaround, add SnapKit to framework search paths so that the compiler can find it but not link it
    s.pod_target_xcconfig = {
      'SWIFT_OPTIMIZATION_LEVEL' => '-Osize',
      'OTHER_LDFLAGS' => '$(inherited) -framework "Flutter"',
      'FRAMEWORK_SEARCH_PATHS' => '$(inherited) "${PODS_ROOT}"/Flutter/** "${PODS_CONFIGURATION_BUILD_DIR}/Flutter"',
      'LIBRARY_SEARCH_PATHS': '$(inherited) "${PODS_ROOT}"/Flutter/** "${PODS_CONFIGURATION_BUILD_DIR}/Flutter"',
    }

    # Build with RxSwift4 compatibility
    #s.xcconfig = { "SWIFT_ACTIVE_COMPILATION_CONDITIONS" => "$(inherited) USE_RXSWIFT4" }
  #end


end
